//Get image
let mainImage = document.getElementById("mainImage");

//Create image array
let imageArray = ["img/1.jpg","img/2.jpg","img/3.jpg", "img/4.png"];

//Set up array index
let imageIndex = 0;

//Set the flag
let stop = true;

//Create function to cycle through images
function changeImage() {
    mainImage.setAttribute("src",imageArray[imageIndex]);
    imageIndex++;
    if(imageIndex >= imageArray.length) {
        imageIndex = 0;
    }
};

//Create play button
let playButton = document.createElement('button');
playButton.innerHTML = 'Play';
playButton.className = "play-btn";
document.body.appendChild(playButton);

//Create pause button
let pauseButton = document.createElement('button');
pauseButton.innerHTML = 'Pause';
pauseButton.className = "pause-btn";
document.body.appendChild(pauseButton);

//Create pause/play functional
let intervalHandle = setInterval(changeImage,10000);

pauseButton.addEventListener("click", () => {
    clearInterval(intervalHandle);
    stop = false;
});

playButton.addEventListener("click", () => {
    if (stop === false) {
        intervalHandle = setInterval(changeImage , 10000);
        stop = true;
    }
});
